import { Controller, Delete, Get, Post, Put } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('getHello')
  getHello(): string {
    return this.appService.getHello();
  }
  // @Post('postHello')
  // postHello(): string {
  //   return 'Hi there!';
  // }
  // @Put('setHello')
  // setHello(): string {
  //   return 'Hello there!';
  // }
  // @Delete('deleteHello')
  // deleteHello(): string {
  //   return 'Sorry there!';
  // }
}
