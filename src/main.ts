import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  const config = new DocumentBuilder()
      .setTitle('NestJS API with MongoDB and Docker Compose and Gitlab CI/CD')
      .setVersion('1.0.14')
      .addServer('http://185.110.189.216:9000/')
      .setDescription('The NestJS API with MongoDB and Docker Compose and Gitlab CI/CD')
      .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  await app.listen(9000).then(()=>{
    console.log('application is running on port 9000');
  });
}
bootstrap();
