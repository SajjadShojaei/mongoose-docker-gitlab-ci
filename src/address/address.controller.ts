import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { AddressDto } from 'src/dto/address.dto';
import { AddressService } from './address.service';

@Controller('address')
export class AddressController {

    constructor(
        private readonly addService:AddressService
    ){}

    @Post('create')
    async createAdd (@Body() createAddDto:AddressDto):Promise<any> {
        return await this.addService.createAdd(createAddDto);
    }

    @Get('findAll')
    async findAllAdd ():Promise<any> {
        return await this.addService.findAllAdd();
    }

    @Get('findOne/:id')
    async findOneAdd (@Param('id') id:string):Promise<any> {
        return await this.addService.findOneAdd(id);
    }
}
